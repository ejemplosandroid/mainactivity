package com.interfaces.mainactivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    RecyclerView rvAlbum;
    AlbumAdapter albumAdapter;
    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ArrayList<Album> albumList = new ArrayList<>();
        albumList.add(new Album("Magna Carta... Holy Grail", R.drawable.jayz, "Magna Carta... Holy Grail1 es el duodécimo álbum de estudio por el rapero estadounidense Jay Z"));
        albumList.add(new Album("Watch the Throne", R.drawable.watchthethrone, " es un álbum de colaboración de los raperos estadounidenses Jay-Z y Kanye West, lanzado el 8 de agosto de 2011, por Roc-A-Fella, Roc Nation, Def Jam Recordings. Las sesiones de grabación se llevaron a cabo en distintos lugares y se comenzó en noviembre del 2010."));
        albumList.add(new Album("American Gangster", R.drawable.american, "American Gangster es el décimo álbum de estudio del rapero Jay-Z lanzado el 6 de noviembre de 2007, a cargo de la discográfica Roc-A-Fella Records."));
        albumList.add(new Album("The blueprint 1", R.drawable.the_blueprient1, "The Blueprint es el sexto álbum de estudio del rapero Jay-Z, lanzado el 11 de septiembre de 2001. Según Nielsen SoundScan, hasta agosto de 2012, The Blueprint había vendido 2 730 000 copias en Estados Unidos."));
        albumList.add(new Album("The blueprint 2", R.drawable.the_blueprient2, "The Blueprint 2: The Gift & The Curse es el séptimo álbum de estudio del rapero Jay Z, lanzado en noviembre del 2002."));
        albumList.add(new Album("The blueprint 3", R.drawable.the_blueprient3, "The Blueprint 3 es el undécimo álbum de estudio del rapero estadounidense Jay-Z, el cual fue lanzado bajo Roc Nation y distribuido por Atlantic Records el martes 11 de septiembre de 2009 en los Estados Unidos y en fechas cercanas alrededor del mundo."));
        albumList.add(new Album("Kingdom come", R.drawable.kingdom_come, "Kingdom Come es el noveno disco de Jay-Z, publicado en 2006."));
        albumList.add(new Album("The black album", R.drawable.black_album, "The Black Album es un álbum del rapero Jay-Z lanzado en 2003. Éste supuestamente se trata de su último álbum de estudio, y generalmente fue bien recibido por los críticos."));

        rvAlbum = findViewById(R.id.rvAlbum);
        albumAdapter = new AlbumAdapter(albumList);
        gridLayoutManager = new GridLayoutManager(this, 2);
        rvAlbum.setLayoutManager(gridLayoutManager);
        rvAlbum.setAdapter(albumAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_second, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
