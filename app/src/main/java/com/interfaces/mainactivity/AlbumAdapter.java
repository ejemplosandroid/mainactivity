package com.interfaces.mainactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    private ArrayList<Album> listAlbum;
    private Album albumSelect;

    public AlbumAdapter(ArrayList<Album> listAlbum) {
        this.listAlbum = listAlbum;
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        final Album album = listAlbum.get(position);
        holder.setData(album);

    }

    @Override
    public int getItemCount() {
        return listAlbum.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivAlbum;
        ImageView ivPlay;
        Album album;

        public AlbumViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAlbum = itemView.findViewById(R.id.ivAlbumItem);
            ivPlay = itemView.findViewById(R.id.play);
            itemView.setOnClickListener(this);
        }

        public void setData(Album album){
            this.album = album;
            ivAlbum.setImageResource(album.getImg());
            if(albumSelect == album){
                ivPlay.setVisibility(View.VISIBLE);
            }else{
                ivPlay.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            albumSelect = album;
            Toast.makeText(view.getContext(), album.getDesc(), Toast.LENGTH_SHORT).show();
            notifyDataSetChanged();
        }
    }
}
